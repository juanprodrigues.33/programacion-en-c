
#include <stdlib.h>
#include <stdio.h>
#define MAX 100
#define N 10
#define random(num) (rand ()% (num))

void mostarMatriz(int matriz[][MAX], int n);
void sumaDeFilas(int matriz[][MAX], int n);
void sumaDeColumnas(int matriz[][MAX], int n);
void sumaDeDiagonal(int matriz[][MAX], int n);
void sumaDeNoDiagonal(int matriz[][MAX], int n);

int main() {
    int i, j, n;
    int matriz[MAX][MAX];

    puts("Ingrese tama�o de matriz cuadrada: \n");
    scanf("%d", &n);

    //gerera mastris cuadrada ramdom
    for (i = 0; i <= n - 1; i++) {
        for (j = 0; j <= n - 1; j++) {
            matriz[i][j] = random(N);
        }
    }

    printf("La matris que usted ingreso es: \n");
    mostarMatriz(matriz, n);
    sumaDeFilas(matriz, n);
    sumaDeColumnas(matriz, n);
    sumaDeDiagonal(matriz, n);
    sumaDeNoDiagonal(matriz, n);
    return 0;
}

void mostarMatriz(int matriz[][MAX], int n) {
    puts("Mostrando matriz desde una funcion\n");
    int i, j;
    for (i = 0; i <= n - 1; i++) {
        for (j = 0; j <= n - 1; j++) {
            printf("%d", matriz[i][j]);
        }
        printf("\n");
    }
}

void sumaDeFilas(int matriz[][MAX], int n) {
    int i, j, k;
    int val = 0;
    for (k = 0; k <= n - 1; k++) {
        for (i = 0; i <= n - 1; i++) {
            for (j = 0; j <= n - 1; j++) {
                if (i == k) {
                    val = val + matriz[i][j];
                    //printf("acomualdo: %d\n", val);
                }
            }
        }
        printf("Suma de la fila %d es : %d \n", k, val);
        val = 0; //setea de nuevo el valor a cero para que sume las suguientes filas restantes
    }
}

void sumaDeColumnas(int matriz[][MAX], int n) {
    int i, j, k;
    int val = 0;
    for (k = 0; k <= n - 1; k++) {
        for (i = 0; i <= n - 1; i++) {
            for (j = 0; j <= n - 1; j++) {
                if (j == k) {
                    val = val + matriz[i][j];
                    //printf("acomualdo: %d\n", val);
                }
            }
        }
        printf("Suma de la columna %d es : %d \n", k, val);
        val = 0; //setea de nuevo el valor a cero para que sume las suguientes filas restantes
    }
}

void sumaDeDiagonal(int matriz[][MAX], int n) {
    int i, j, k;
    int val = 0;
    
    for (i = 0; i <= n - 1; i++) {
        for (j = 0; j <= n - 1; j++) {
            if (i == j) {
                val = val + matriz[i][j];
                //printf("acomualdo: %d\n", val);
            }
        }
    }
    printf("Suma de la Diagonal principal es : %d \n", val);
}

void sumaDeNoDiagonal(int matriz[][MAX], int n) {
    int i, j, k;
    int val = 0;

    for (i = 0; i <= n - 1; i++) {
        for (j = 0; j <= n - 1; j++) {
            if (i != j) {
                val = val + matriz[i][j];
                //printf("acomualdo: %d\n", val);
            }
        }
    }
    printf("Suma de elementos que no estan en al Diagonal principal es : %d \n", val);


}


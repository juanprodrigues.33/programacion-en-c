#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#define MAXIMA_LONGITUD_CADENA 100

void removerCaracteres(char *cadena, char *caracteres);

// La funcion para ordenar cadenas, una simple envoltura de strcmp
int comparar(const void *a, const void *b);

// La funcion que nos interesa
int esAnagrama(char *cadena, char *otraCadena);

int main() {
  // Un arreglo de arreglos de cadenas, solo es para ejemplificar
  char palabras[5][2][MAXIMA_LONGITUD_CADENA] = {
      {"I am Lord Voldemort", "Tom Marvolo Riddle"},
      {"Amor", "Roma"},
      {"Luis", "luis"},
      {"Esto no es", "un anagrama"},
      {"Frase", "Fresa"}};
  for (int x = 0; x < 5; x++) {
    printf("\n\nProbando con '%s' y '%s'\n", palabras[x][0], palabras[x][1]);
    int resultado = esAnagrama(palabras[x][0], palabras[x][1]);
    if (resultado) {
      printf("SON anagramas");
    } else {
      printf("NO SON anagramas");
    }
  }
  return 0;
}

int comparar(const void *a, const void *b) {
  // Castear a char y regresar lo que regresaria strcmp ;)
  return strcmp((char *)a, (char *)b);
}

void removerCaracteres(char *cadena, char *caracteres) {
  int indiceCadena = 0, indiceCadenaLimpia = 0;
  int deberiaAgregarCaracter = 1;
  // Recorrer cadena caracter por caracter
  while (cadena[indiceCadena]) {
    // Primero suponemos que la letra si debe permanecer
    deberiaAgregarCaracter = 1;
    int indiceCaracteres = 0;
    // Recorrer los caracteres prohibidos
    while (caracteres[indiceCaracteres]) {
      // Y si la letra actual es uno de los caracteres, ya no se agrega
      if (cadena[indiceCadena] == caracteres[indiceCaracteres]) {
        deberiaAgregarCaracter = 0;
      }
      indiceCaracteres++;
    }
    // Dependiendo de la variable de arriba, la letra se agrega a la "nueva
    // cadena"
    if (deberiaAgregarCaracter) {
      cadena[indiceCadenaLimpia] = cadena[indiceCadena];
      indiceCadenaLimpia++;
    }
    indiceCadena++;
  }
  // Al final se agrega el carácter NULL para terminar la cadena
  cadena[indiceCadenaLimpia] = 0;
}

int esAnagrama(char *palabra, char *otraPalabra) {

  // Quitar caracteres como signos de puntuacion o espacios
  removerCaracteres(palabra, " ,.!");
  removerCaracteres(otraPalabra, " ,.!");

  // Convertir a minassculas a ambas palabras
  int contador = 0;
  while (palabra[contador]) {
    palabra[contador] = tolower(palabra[contador]);
    contador++;
  }
  contador = 0;
  while (otraPalabra[contador]) {
    otraPalabra[contador] = tolower(otraPalabra[contador]);
    contador++;
  }

  // Ordenar las letras de ambas palabras
  qsort(otraPalabra, strlen(otraPalabra), sizeof(char), comparar);
  qsort(palabra, strlen(palabra), sizeof(char), comparar);

  // Si al ordenarlas y ponerlas en minasculas son iguales, entonces son
  // anagramas
  return strcmp(palabra, otraPalabra) == 0;
}

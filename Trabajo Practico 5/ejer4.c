
#include <stdlib.h>
#include <stdio.h>
#define MAX 100
#define N 5
#define random(num) (rand ()% (num))

void mostarMatriz(int matriz[MAX][MAX], int n, int m);
void productoMatriz(int matrizA[MAX][MAX], int matrizB[MAX][MAX], int n, int m, int p);

int main() {
    int i, j, n, m, p;
    int matrizA[MAX][MAX];
    int matrizB[MAX][MAX];
 

    puts("Ingrese numero de filas de matriz A : ");
    scanf("%d", &n);
    puts("Ingrese numero de columnas de matriz A : ");
    scanf("%d", &m);
    printf("Ingrese numero de columnas de matriz B porque el numero de columnas es %d : \n", m);
    scanf("%d", &p);

    //gerera mastriz ramdom

    for (i = 0; i <= n - 1; i++) { //fila
        for (j = 0; j <= m - 1; j++) { //columna
            matrizA[i][j] = random(N);
        }
    }
    for (i = 0; i <= m - 1; i++) { //fila
        for (j = 0; j <= p - 1; j++) { //columna
            matrizB[i][j] = random(N);
        }
    }

    printf("La matris que usted ingreso es: \n");
    mostarMatriz(matrizA, n, m);
    mostarMatriz(matrizB, m, p);
    
    // funciona el genarado de matrices ramdom

    productoMatriz(matrizA, matrizB, n, m, p);


    return 0;
}

void mostarMatriz(int matriz[MAX][MAX], int n, int m) {
    puts("Mostrando matriz desde una funcion: ");
    int i, j;
    for (i = 0; i <= n - 1; i++) {
        for (j = 0; j <= m - 1; j++) {
            printf("%d ", matriz[i][j]);
        }
        printf("\n");
    }
}

void productoMatriz(int matrizA[MAX][MAX], int matrizB[MAX][MAX], int n, int m, int p) {
    //matriz C tiene que ser nxp 
    int matrizC[n][p];
    int a,i,j,suma;
    // Necesitamos hacer esto por cada columna de la segunda matriz (B)
    for ( a = 0; a < p; a++) {
        for ( i = 0; i < n; i++) {
             suma = 0;
            for ( j = 0; j < m; j++) {
                // Multiplicamos y sumamos resultado
                suma += matrizA[i][j] * matrizB[j][a];
            }
            // Lo acomodamos dentro del la matrizC
            matrizC[i][a] = suma;
        }
    }
    //mostramos el resultado de producto de matrices
    mostarMatriz(matrizC,n,p);
}




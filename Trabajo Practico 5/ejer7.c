#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#define max 100
#define vocal 6
int contarNumeros(char cadena[max]);
int iniciaNumero(char cadena[max]);
int numeroDeCadena(char s1[]);

void main(void) {
    char s1[max] = "Linea con texto 100 y numeros";
    char s2[max] = "Esta segunda linea tiene varias palabras y un numero 200.";
    char s3[max] = "La tercer linea tiene el numero 2 y palabras";

    int suma = numeroDeCadena(s1) + numeroDeCadena(s3) + numeroDeCadena(s2);
    puts("################################################################");
    
    
    printf("suma total que hay en las cadenas de caracteres: %d\n", suma);
}

int numeroDeCadena(char cadena[]) {
    int longitud = strlen(cadena);
    printf("Tamanio de la cadena: %d\n", longitud);
    int i;
    int numerosTotal = contarNumeros(cadena);
    printf("Numeros que hay en la cadena: %d\n", numerosTotal);
    int numerosInicia = iniciaNumero(cadena);
    printf("Donde inicia el numero: %d\n", numerosInicia);
    int j;

    //--------------------------------------------------	
    char src[50], dest[50] = "", copiando[50];
    strcpy(copiando, "");
    //--------------------------------------------------

    for (i = 0; i < (longitud - 1); i++) {
        if (i == numerosInicia) {
            for (j = numerosInicia; j < numerosTotal + numerosInicia; j++) {
                printf("valor dentro del for: %c \n", cadena[j]);
                strncat(copiando, & cadena[j], 1);
            }
        }

    }
    strcat(dest, copiando);
    for (j = 0; j < numerosTotal; j++) {
        printf("valor copiado:%c \n", copiando[j]);
    }
    int comoEntero = atoi(dest);

    printf("Concadenando: %d\n", comoEntero);
    puts("--------------------------------------------------");
    return comoEntero;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int contarNumeros(char cadena[max]) {
    char numeros[] = "0123456789";
    int i, j, contNumeros = 0;
    for (i = 0; i < strlen(cadena) - 1; i++) {
        for (j = 0; j < sizeof numeros - 1; j++) {
            if (cadena[i] == numeros[j]) {
                // printf("Numero %c letra %c contador &d ",cadena1[i],vocales[j],contNumeros );
                contNumeros++;
            }
        }
    }
    return contNumeros;
}

int iniciaNumero(char cadena[max]) {
    char numeros[] = "0123456789";
    int i, j, contNumeros = 0, terminaNumero = 0;
    for (i = 0; i < strlen(cadena) - 1; i++) {
        for (j = 0; j < sizeof numeros - 1; j++) {
            if (cadena[i] == numeros[j]) {
                terminaNumero = i;
                contNumeros++;
            }
        }
    }
    return (terminaNumero - contNumeros + 1);
}

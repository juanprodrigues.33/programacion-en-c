#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max 100
#define vocal 6
int contar(char cadena[max], char info[6]);
int contarVocales(char cadena[max]);
int contarNumeros(char cadena[max]);
int contarEspacios(char cadena[max]);

void main(void) {
    char s1[100];
    char* s2;
    int n;
    printf("Cadena a analizar?:");
    fgets(s1, sizeof (s1), stdin);

    int vocales = contarVocales(s1);
    int numeros = contarNumeros(s1);
    int espacios = contarEspacios(s1);
    int consonantes = strlen(s1) - vocales - numeros - espacios - 1;
    printf("Numero de vocales: %d \n", vocales);
    printf("Numero de numeros: %d \n", numeros);
    printf("Numero de consonantes: %d \n", consonantes);
    printf("Numero de espacios: %d \n", espacios);

}



int contarVocales(char cadena[max]) {
    char vocales[] = "aeiou";
    int i, j, contVocales;
    for (i = 0; i < strlen(cadena) - 1; i++) {
        for (j = 0; j < sizeof vocales - 1; j++) {
            if (cadena[i] == vocales[j]) {
                contVocales++;
            }

        }
    }
    return contVocales;

}

int contarNumeros(char cadena[max]) {
    char vocales[] = "0123456789";
    int i, j, contNumeros = 0;
    for (i = 0; i < strlen(cadena) - 1; i++) {
        for (j = 0; j < sizeof vocales - 1; j++) {
            if (cadena[i] == vocales[j]) {
                // printf("Numero %c letra %c contador &d ",cadena1[i],vocales[j],contNumeros );
                contNumeros++;

            }

        }
    }
    return contNumeros;
}

int contarEspacios(char cadena[max]) {
    char vocales[] = " ";
    int i, j, contVocales = 0;
    for (i = 0; i < strlen(cadena) - 1; i++) {
        for (j = 0; j < sizeof vocales - 1; j++) {
            if (cadena[i] == vocales[j]) {

                contVocales++;
            }

        }
    }
    return contVocales;
}








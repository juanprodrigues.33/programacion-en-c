
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14159265358979323846

void radianesRectangulares(float r, float theta) {
    double x = r * cos(theta);
    double y = r * sin(theta);
    printf("En coordenadas cartesianas: \n");
    printf("x= %.5f\n", x);
    printf("y= %.5f\n", y);
}

int main() {
    float r, theta;
    printf("Ingrese la coordenada r : \n");
    scanf("%f", &r);

    printf("Ingrese la coordenada theta(angulo) : \n");
    scanf("%f", &theta);
    printf("%f", theta);
    theta = (theta * PI) / 180;
    printf("%f", theta);
    radianesRectangulares(r, theta);

    return (0);
}


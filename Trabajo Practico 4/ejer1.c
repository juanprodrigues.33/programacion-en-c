
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/*
Analice qué hace el siguiente bucle while, luego reescribalo utilizando for y do-while
num = 10;
while (num <= 100) {
printf("%d \n",num);
num += 10;
}
 */
char tipoDeEntero(int n){
    char var='P';
    if (n>0){
         return (var='P');
    }else if(n<0){
           return (var='N');
    }else if(n==0){
       return (var='C');
    
    }
}
int main() {
    int n;
    printf("Ingrese un numero entero: \n");
    scanf("%d", &n);
    printf("El numero ingresado es: %c \n",tipoDeEntero(n));
         
    return (0);

}

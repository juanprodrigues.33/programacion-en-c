
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/*
Analice qué hace el siguiente bucle while, luego reescribalo utilizando for y do-while
num = 10;
while (num <= 100) {
printf("%d \n",num);
num += 10;
}
 */
void esDivisible(int n, int m) {
    char varTrue[]="true";
    char varFalse[]="false";

        if (n % m == 0) {
            printf(varTrue);
                    
        } else {

             printf(varFalse);
        }

}

int main() {
    int n, m;
    printf("Ingrese un numero entero como numerador: \n");
    scanf("%d", &n);
    printf("Ingrese un numero entero como denominador: \n");
    scanf("%d", &m);
    esDivisible(n, m);
/*
    printf("Divisible: %s \n", esDivisible(n, m));
*/

    return (0);

}

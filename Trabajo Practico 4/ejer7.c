/*
De la pr�ctica anterior (unidad 3), convierta el c�digo de Monte Carlo en una funci�n
que reciba la cantidad de iteraciones/intentos como par�metro y devuelva la
aproximaci�n de PI como resultado. Ejecute dicha funci�n dentro de un bucle for con 5
o 6 potencias de 10. Imprimiendo cada una de las aproximaciones de PI.
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float valorDePI;

void calculoDePi(int contar, int intentos) {

    valorDePI = 4 * ((float) contar / (float) intentos);
    printf("valor de pi=%f \n", valorDePI);

}

int contarIntentos(int intentos) {
    int n = 0;
    int contar = 0;
    while (n <= intentos) {

        float x = -1 + rand() / (float) (RAND_MAX / 2);

        float y = -1 + rand() / (float) (RAND_MAX / 2);

        printf("Valor de x=%f y el valor de y=%f. \n", x, y);
        if (x * x + y * y <= 1) {
            puts("Ingreso la condicion.\n");
            contar++;
        }
        n++;
    }
     printf("veces que entro=%d, intentos que se realizo=%d\n", contar, intentos);
     
     return (contar);
}

int main() {
    int intentos;

    printf("Ingrese numero de intentos: \n");
    scanf("%d", &intentos);

    int contar=contarIntentos(intentos);
   
    calculoDePi(contar, intentos);


    return (0);
}


#include <stdio.h>
#include <stdlib.h>

void main(void) {
    int n1, n2, a, b, resultado;

    printf("Ingrese el primer numero\n");
    scanf("%d", &n1);
    printf("Ingrese el segundo numero\n");
    scanf("%d", &n2);

    if (n1 > n2) {
        a = n1;
        b = n2;
    } else {
        a = n2;
        b = n1;
    }

    do {
        resultado = b;
        b = a % b;
        a = resultado;
    } while (b != 0);

  
    printf("El M.C.D. entre %d y %d es: %d", n1, n2, resultado);

}

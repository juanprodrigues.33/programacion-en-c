
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14159265358979323846

void mensajeConversion(char res[], float b) {
    printf("El valor ingresado equivale a %f %s \n",b, res);
}

void kilometrosMillas() {
    char conversion[] = "Millas";
    float a, b;
    puts("Ingrese valor en Km: \n");
    scanf("%f", &a);
    b = a;
    mensajeConversion(conversion, b);
}

void millasKilometros() {
    char conversion[] = "Kilimetros";
    float a, b;
    puts("Ingrese valor en Millas: \n");
    scanf("%f", &a);
    b = a;

    mensajeConversion(conversion, b);
}

void kilogramosOnzas() {
    char conversion[] = "Onzas";
    float a, b;
    puts("Ingrese valor en Kilogramos: \n");
    scanf("%f", &a);
    b = a;
    mensajeConversion(conversion, b);
}

void onzasKilogramos() {
    char conversion[] = "Kilogramos";
    float a, b;
    puts("Ingrese valor en Onzas: \n");
    scanf("%f", &a);
    b = a;
    mensajeConversion(conversion, b);
}

void celsiusFahrenheit() {
    char conversion[] = "Fahrenheit";
    float a, b;
    puts("Ingrese valor en Celsius: \n");
    scanf("%f", &a);
    b = a;
    mensajeConversion(conversion, b);
}

void fahrenheitCelsius() {
    char conversion[] = "Celsius";
    float a, b;
    puts("Ingrese valor en fahrenheit: \n");
    scanf("%f", &a);
    b = a;
    mensajeConversion(conversion, b);
}

void centimetrosPulgadas() {
    char conversion[] = "Pulgadas";
    float a, b;
    puts("Ingrese valor en cm: \n");
    scanf("%f", &a);
    b = a;
    mensajeConversion(conversion, b);
}

void pulgadasCentimetros() {
    char conversion[] = "Centimetros";
    float a, b;
    puts("Ingrese valor en pulgadas: \n");
    scanf("%f", &a);
    b = a;
    mensajeConversion(conversion, b);
}

void menu() {
    char opcion;
    do {

        puts("Ingrese el numero entre parentesis para la conversion que desea realizar: \n");
        puts("- (1) kilometros a millas\n");
        puts("- (2) millas a kilometros\n");
        puts("- (3) kilogramos a onzas\n");
        puts("- (4) onzas a kilogramos\n");
        puts("- (5) celsius a fahrenheit\n");
        puts("- (6) fahrenheit a celsius\n");
        puts("- (7) centimetros a pulgadas\n");
        puts("- (8) pulgadas a centimetros\n");
        puts("- (9) Ingrese cualquier otro carácter para salir.\n");

        scanf("%c", &opcion);
        switch (opcion) {
            case '1':kilometrosMillas();
                break;
            case '2':millasKilometros();
                break;
            case '3':kilogramosOnzas();
                break;
            case '4':onzasKilogramos();
                break;
            case '5':celsiusFahrenheit();
                break;
            case '6':fahrenheitCelsius();
                break;
            case '7':centimetrosPulgadas();
                break;
            case '8':pulgadasCentimetros();
                break;
            default:
                opcion='S';
                break;


        }
    } while (opcion != 'S');
}

int main() {

    menu();
    return (0);
}

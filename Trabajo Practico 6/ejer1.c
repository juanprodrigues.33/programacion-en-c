/*
1. Funciones y punteros
Cree una función que reciba tres parámetros de tipo entero y punteros (a1, a2 y a3). 
Dentro de la función compare los tres números, el mayor número lo debe guardar en a1 y el menor en a3.
La función no debe devolver valores con return sino modificar directamente utilizando los
punteros. Desde main imprima los valores de a1, a2 y a3 antes de llamar la función y luego de
llamarla.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void funcion(int *a1, int *a2, int *a3);
int funcionmax(int fijo, int aa, int bb, int cc);

void main(void) {
    int a1 = 3;
    int a2 = 20;
    int a3 = 10;
    printf("valores antes de funcion: a1=%d, a2=%d, a3=%d \n", a1, a2, a3);
    funcion(&a1, &a2, &a3);
    printf("valores despues de la funcion: a1=%d, a2=%d, a3=%d \n", a1, a2, a3);

}

void funcion(int *a1, int *a2, int *a3) {

    int max = *a1;
    int min = *a1;
    int valores[] = {*a1, *a2, *a3};

    int i;
    for (i = 0; i < 3; i++) {
        printf(" valores que va iterando %d\n",valores[i]);
        
        if (valores[i] >= max) {
            max = valores[i];
        }
        if (valores[i] <= min) {
            min = valores[i];
        }

    }
    *a1 = max;
    *a3 = min;
    printf("valores dentro de la funcion: a1=%d, a2=%d, a3=%d \n", (*a1), (*a2), (*a3));


}



#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
De la práctica 4 (funciones), convierta el código del ejercicio 3 (conversor de Celsius a
Fahrenheit) para que todas las funciones utilicen punteros en los parámetros.
 */

//variables de grado global
char grados;
float *celsios, *fahrenheit;

void celsiusFaranheit() {
    float temp;
    printf("Ingrese grados en Celsios\n");
    scanf("%f", &temp);
    celsios = &temp;
    temp = 1.8 * (*celsios) + 32;
    fahrenheit = &temp;
    printf("Resultado\n");
    printf("Grados en Fahrenheit: %f\n", *fahrenheit);
}

void fahrenheitCelsius() {
    float temp;
    printf("Ingrese grados en Fahrenheit\n");
    scanf("%f", &temp);
    fahrenheit = &temp; //asigno el valor de temp a fahrenheit porque si lo dejo en el scanf me da un error
    temp = ((*fahrenheit) - 32)*5 / 9; //asigno el valor de la cuenta ala variable que apunta a celsios
    celsios = &temp;
    printf("Resultado\n");
    printf("Grados en Celsios%f\n", *celsios);
}

void ingresoDatos() {


    printf("Que calculadora quiere selecionar:\n");
    puts("C) Celsius-->Fahrenheit \n");
    puts("F) Fahrenheit-->Celsius \n");
    scanf("%c", &grados);
    //validamos que grados se pueda escribir en mayusculas y numusculas
    if (grados == 'c' || grados == 'C') {
        grados = 'C';
    } else if (grados == 'F' || grados == 'f') {
        grados = 'F';
    }
    switch (grados) {
        case 'C':
            celsiusFaranheit();
            break;
        case 'F':
            fahrenheitCelsius();
            break;
        default:puts("ERROR\n");
            break;


    }
}

int main() {
    ingresoDatos();
    return (0);
}
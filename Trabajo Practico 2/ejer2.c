//Eval�e el resultado de cada expresi�n aritm�tica, puede utilizar la tabla de precedencia de
//operadores vista en la teor�a. Luego puede escribir cada expresi�n en un archivo de c�digo C y
//comprobar el resultado.
//a. 6 + 2 * 3 - 4 / 2
//b. 5 * (5 + (6 - 2) + 1)
//c. 7 - 6 / 3 + 2 * 3 / 2 - 4 / 2
//d. 7 * 10 - 5 / 3 * 4 + 9
int main(){
 	double a=6 + 2 * 3 - 4 / 2;
	double b= 5 * (5 + (6 - 2) + 1);
	double c= 7 - 6 / 3 + 2 * 3 / 2 - 4 / 2;
	double d=7 * 10 - 5 / 3 * 4 + 9;
        printf("%f\n",a);
        printf("%f\n",b);
        printf("%f\n",c);
        printf("%f\n",d);
return 0;
}


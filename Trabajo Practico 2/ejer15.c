
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define G 6.674E-8

int main() {
    float m1, m2, d;
    
    printf("Ingrese masa 1: \n");
    scanf("%f", &m1);
    printf("Ingrese masa 2: \n");
    scanf("%f", &m2);
    printf("Ingrese distancia: \n");
    scanf("%f", &d);
    double F = (G*m1*m2)/pow(d, 2);
    printf("Fuerza: %f ", F);
    return (0);
}



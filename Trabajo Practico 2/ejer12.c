#include <stdio.h>
#include <math.h>
#include <stdlib.h>
int main(){
    int a, b;
    printf("Ingrese un numero: \n");
    scanf("%d", &a);
    printf("Ingrese otro numero: \n");
    scanf("%d", &b);
    int suma = a + b;
    int resta = a - b;
    int producto = a*b;
    double potencia = pow(a, b);
    printf("suma: %d \n", suma);
    printf("resta: %d \n", resta);
    printf("producto: %d \n", producto);
    printf("Potencia a^b: %f \n", potencia);
    return(0);
}

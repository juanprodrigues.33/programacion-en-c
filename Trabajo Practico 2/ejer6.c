//
//6. Expresiones aritm�ticas
//Escriba las siguientes expresiones aritm�ticas en c�digo C (todas en un mismo archivo), asigne
//a las variables a, b, c, d, x, y, z, m, n valores int o float a su elecci�n. Tenga en cuenta que
//algunas variables est�n en operaciones de divisi�n, puede ser necesario que sean de tipo float.
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
int main (){

    float x,y,z,a,b,c,d,m,n;
    printf("Ingrese valores de x y z a b c d m n \n");
    scanf("%f",&x,&y,&z,&a,&b,&c,&d,&m,&n);
    puts("Evaluamos los valores ingresados en las expreciones.\n");
    float a1=x/y+1;
    float b1= (x+y)/(x-y);
    float c1=(x+y/z)/(x+y/z); //en teoria tendria que ser 1;
    float d1=b/(c+d);
    float e=(a+b)*c/d;
    float f=pow(pow((a+b),2),2);
    float g=(x+y)/(1-4*x);
    float h=x*y/(m*n);
    float i=powf((x+y,2),2)*(a-b);
    
    printf("El valor de a) %f\n",a1);
    printf("El valor de b) %f\n",b1);
    printf("El valor de c) %f\n",c1);
    printf("El valor de d) %f\n",d1);
    printf("El valor de e) %f\n",e);
    printf("El valor de f) %f\n",f);
    printf("El valor de g) %f\n",g);
    printf("El valor de h) %f\n",h);
    printf("El valor de i) %f\n",i);
// Resultado por portalla en dev-C++
//El valor de a) 1.#INF00
//El valor de b) 1.000000
//El valor de c) 1.000000
//El valor de d) -1.#QNAN0
//El valor de e) -1.#QNAN0
//El valor de f) 0.000000
//El valor de g) -0.333333
//El valor de h) -1.#IND00
//El valor de i) -0.000000

/*
Acortando los decimales , detodas formas da como resultado esos valores
Si lo hago por nerbeas me da perfecto!!!.
*/

return(0);
}


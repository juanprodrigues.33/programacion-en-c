#define PI 3.14159265358979323846
#include <stdio.h>
#include <math.h>
int main(){
	int radio_1=5; 
	float radio_2=12.5;
//	volumen de esdera 4*pi *r4/3
	float vol_1=4*PI*pow(radio_1,3)/3;
	float vol_2=4*PI*pow(radio_2,3)/3;
	printf("Volumen de esfera 1: %f\n",vol_1);
	printf("Volumen de esfera 2: %f\n",vol_2);
return(0);
}

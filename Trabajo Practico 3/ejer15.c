/*
Un m�todo cient�fico com�n para estimar valores desconocidos se conoce como el
m�todo de Monte Carlo, nombrado por el famoso casino. El concepto b�sico es usar la
aleatoriedad para resolver problemas, en este ejercicio queremos usar el m�todo de
Monte Carlo para estimar p.

a. Usando un programa podemos simular un lanzamiento de dardo en un c�rculo de
radio 1, centrado alrededor (0, 0) como se muestra a continuaci�n.

b. Simulamos esto generando una gran cantidad de coordenadas (x, y) contenidas
dentro de un cuadrado de longitud de lado = 2.

c. Luego contamos el n�mero de veces que las coordenadas se encuentran dentro
del c�rculo de radio = 1. Eso es cuando x^2 + y^2 <= 1.

d. Debido a la aleatoriedad de nuestros lanzamientos, esperamos que la proporci�n
de puntos acertados/intentos sea igual a la proporci�n de el �rea del
c�rculo / cuadrado (o, p/4). Por lo tanto, nuestra estimaci�n de p est�
dada por p = 4.0 * (aceptado / intentos).

e. Los n�meros aleatorios entre -1 y 1 se pueden generar de la siguiente manera:
#include <stdlib.h>
float r = -1 + rand() / (float) (RAND_MAX / 2);
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
    int intentos;
    int contar = 0;
    int n = 0;
    float valorDePI;
    printf("Ingrese numero de intentos: \n");
    scanf("%d", &intentos);

    while (n <= intentos) {

        float x = -1 + rand() / (float) (RAND_MAX / 2);

        float y = -1 + rand() / (float) (RAND_MAX / 2);
 
        printf("Valor de x=%f y el valor de y=%f. \n", x, y);
        if (x * x + y * y <= 1) {
            puts("Ingreso la condicion.\n");
            contar++;
        }
        n++;
    }
    printf("veces que entro=%d, intentos que se realizo=%d\n", contar, intentos);
    
    valorDePI = 4 * ((float) contar / (float) intentos);
    printf("valor de pi=%f \n", valorDePI);


    return (0);
}





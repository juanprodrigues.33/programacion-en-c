/*
�Qu� valor tendr� consumo si velocidad es igual a 120?

if (velocidad > 80)
	consumo = 10.00;
else if (velocidad > 100)
	consumo = 12.00;
else if (velocidad > 120)
	consumo = 15.00;
	
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
ingresa a la primera opcion ya que , el if(concadenado con else if) funciona lee de arriba para abajo
y si encuentra la primera condicion que es verdadera INGRESA, y sale del bucle if 
*/

int main() {
	float velocidad,consumo;
	velocidad=120.0;
if (velocidad > 80)
	consumo = 10.00;
else if (velocidad > 100)
	consumo = 12.00;
else if (velocidad > 120)
	consumo = 15.00;

printf("hola para: %f\n",consumo);
	return(0);
}
	


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*

Escriba un c�digo que reciba un n�mero ingresado por el usuario, luego indique si dicho
n�mero es positivo, negativo o cero, y si es par o impar.
*/
int main() {
    int num;
    printf("Ingrese un numero entero: \n");
    scanf("%d",&num);
    if (num>0){
        puts("El numero ingresado es positivo");
    }else if(num<0){
        puts("El numero ingresado es negativo");
    }else if(num==0){
        puts("El numero ingresado es cero");
    }
    return (0);
}




#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
Escriba un programa que solicite al usuario el ingreso de una letra del abecedario, si
esta es una vocal debe devolver Verdadero, en cualquier otro caso Falso.
 */
int main() {
    char letra;
    printf("Ingrese letra del Abecedario\n");
    scanf("%c", &letra);
    //propongo las mayusculas y minusculas de las vocales
    switch (letra) {
        case 'A':
            printf("Verdadero");
            break;
        case 'a':
            printf("Verdadero");
            break;
        case 'E':
            printf("Verdadero");
            break;
        case 'e':
            printf("Verdadero");
            break;
        case 'i':
            printf("Verdadero");
            break;
        case 'I':
            printf("Verdadero");
            break;
        case 'o':
            printf("Verdadero");
            break;
        case 'O':
            printf("Verdadero");
            break;
        case 'u':
            printf("Verdadero");
            break;
        case 'U':
            printf("Verdadero");
            break;
        default:puts("Falso\n");
            break;


    }


    return (0);
}



#include <stdio.h>
#include <stdlib.h>

void main(void) {
    int i,j,n=5,m=3;
    
    //imprime un triangulo de base 4 l altura 4
    for (i = 0; i < n; i++) {
        for (j = 0; j < i; j++) {
            putchar('*');
        }
        putchar('\n');
    }
    
    //imprime un rectangulo de base 3 y altura 5
    for (i = n; i > 0; i--) {
        for (j = m; j > 0; j--) {
            putchar('*');
        }
        putchar('\n');
    }
}


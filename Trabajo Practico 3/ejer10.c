
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/*
Analice qu� hace el siguiente bucle while, luego reescribalo utilizando for y do-while
num = 10;
while (num <= 100) {
printf("%d \n",num);
num += 10;
}
 */
int main() {
    int num = 10;
    while (num <= 100) {
        printf("%d \n", num);
        num += 10;
    }
    int i;
    
    //bucle for
    for (i = 10; i <= 100; i += 10) {
        printf("%d \n", i);

    } 
    //bucle do-while
    num = 10;
    do {
       
        printf("%d \n", num);
        num += 10;
    } while (num <= 100);

    return (0);

}


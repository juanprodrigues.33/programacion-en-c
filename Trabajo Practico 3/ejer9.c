
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14159265358979323846

/*
Tienes tres dados est�ndar. Calcule cu�ntas formas existen para lograr un total de 10
entre los tres dados. Luego calcule la probabilidad de tirar un diez.
 */
int main() {
  
    int n = 0, m = 0;
    int i, j, k;


    for (i = 1; i < 7; i++) {

        for (j = 1; j < 7; j++) {

            for (k = 1; k < 7; k++) {
                printf("dado 1: %d\n", i);
                printf("dado 2: %d\n", j);
                printf("dado 3: %d\n", k);
                if ((i + j + k) == 10) {
                    puts("-----------------------\n");
                    printf("dado 1: %d\n", i);
                    printf("dado 2: %d\n", j);
                    printf("dado 3: %d\n", k);
                    n = n + 1; //casos favorables
                    puts("-----------------------\n");

                }
                m++; //casos posibles

            }

        }

    }

    float probabilidad = (float) n / (float) m * 100; //casos favorable sobre posibles
    printf("Casos Favorables: %d\n", n);
    printf("Casos Desfavorables: %d\n", m);
    printf("Probabilidad de obtener un 10: %.2f%% \n", probabilidad);


    return (0);
}


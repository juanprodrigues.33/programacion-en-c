
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


/*
En un pa�s se utilizan diferentes tasas impositivas seg�n el sueldo bruto anual del
contribuyente. Los contribuyentes casados suman sus ingresos y pagan impuestos
sobre el total. La tabla muestra los c�lculos de la tasa de impuestos.

Categoria| Sueldo bruto anual     |Tasa impositiva
    A    | Menor o igual a 300000 | 3%
    B    | Entre 300000 y 450000  | 8%
    C    | Entre 450000 y 700000  | 13%
    D    | Entre 700000 y 1200000 | 20%
    E    | M�s de 1200000         | 35%
a) Dise�e un proceso de decisi�n que calcule el impuesto a pagar teniendo en cuenta
las siguientes entradas: soltero/a o casado/a, si es casado/a debe indicar dos sueldos,
si es soltero/a uno.
b) El c�digo debe indicar en qu� categor�a est� el contribuyente, cu�nto impuesto
deber� pagar y cu�l es el sueldo mensual.
 */
int main() {

    int situacion;
    int sueldo;
    char categoria;
    int sueldoPareja;
    int impuesto;
    int sueldoConDescuento;
    printf("Ustes es casado?\n");
    printf("1)Si.\n");
    printf("2)No.\n");
    scanf("%d", &situacion);

    if (situacion == 1) {

        printf("Usted seleciono casado\n");
        printf("Ingrese su sueldo ANUAL\n");
        scanf("%d", &sueldo);
        printf("Ingrese el sueldo ANUAL de su pareja\n");
        scanf("%d", &sueldoPareja);
        //reutilizamos la variable sueldo y la reescribimos para, dondo ahora toma el sueldo neto de ambos
        sueldo = sueldo + sueldoPareja;
    } else {
        printf("Ingrese un SUELDO ANUAL:");
        scanf("%d", &sueldo);
    }
    if (300000 >= sueldo) {
        impuesto = sueldo * 0.03;
        sueldoConDescuento = sueldo - impuesto;
        categoria = 'A';
    } else if (300000 >= sueldo && sueldo < 450000) {
        impuesto = sueldo * 0.08;
        sueldoConDescuento = sueldo - impuesto;
        categoria = 'B';
    } else if (450000 >= sueldo && sueldo < 700000) {
        impuesto = sueldo * 0.13;
        sueldoConDescuento = sueldo - impuesto;
        categoria = 'C';
    } else if (700000 >= sueldo && sueldo < 1200000) {
        impuesto = sueldo * 0.20;
        sueldoConDescuento = sueldo - impuesto;
        categoria = 'D';
    } else if (1200000 <= sueldo) {
        impuesto = sueldo * 0.35;
        sueldoConDescuento = sueldo - impuesto;
        categoria = 'F';
    }
    printf("Resultados\n");
    
    printf("Categoria: %c\n", categoria);
    printf("Impuestos: %d\n", impuesto);
   float sueldomensual=sueldoConDescuento / 12;
    printf("Sueldo mensual: %f\n", sueldomensual);

    return (0);
}



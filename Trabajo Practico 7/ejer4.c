
/*
Un n�mero racional se caracteriza por el numerador y el denominador. Escribir un programa
para operar con n�meros racionales. Las operaciones a definir son la suma, resta,
multiplicaci�n y divisi�n;
 * 
 * 
 *  adem�s de una funci�n para simplificar cada n�mero racional
 */
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#define MAXIMA_LONGITUD_CADENA 100

struct numRacional {
    int denominador;
    int numerador;
};
void operaciones();
void suma(struct numRacional num1, struct numRacional num2);
void resta(struct numRacional num1, struct numRacional num2);
void producto(struct numRacional num1, struct numRacional num2);
void division(struct numRacional num1, struct numRacional num2);
void simplificar(int M, int N);

void operaciones() {
    struct numRacional num1;
    struct numRacional num2;
    puts("Ingrese numerador n1 \n");
    scanf("%d", &num1.numerador);
    puts("Ingrese  deniminador de n1\n");
    scanf("%d", &num1.denominador);
    puts("Ingrese numerador n2 \n");
    scanf("%d", &num2.numerador);
    puts("Ingrese  deniminador de n2\n");
    scanf("%d", &num2.denominador);
    suma(num1, num2);
    resta(num1, num2);
    producto(num1, num2);
    division(num1, num2);

}

void suma(struct numRacional num1, struct numRacional num2) {
    int numerador = num1.numerador * num2.denominador + num1.denominador * num2.numerador;
    int denominador = num1.denominador * num2.denominador;
    
    printf("Suma: %d/%d \n ",  numerador,denominador);
    simplificar(numerador,denominador);

}

void resta(struct numRacional num1, struct numRacional num2) {
    int numerador = num1.numerador * num2.denominador - num1.denominador * num2.numerador;
    int denominador = num1.denominador * num2.denominador;
    
    printf("Resta: %d/%d \n ", numerador,denominador);
    simplificar(numerador,denominador);
}

void producto(struct numRacional num1, struct numRacional num2) {
    int numerador = num1.numerador * num2.numerador;
    int denominador = num1.denominador * num2.denominador;
    
    printf("Producto: %d/%d \n ", numerador,denominador);
    simplificar(numerador,denominador);
}

void division(struct numRacional num1, struct numRacional num2) {
    int numerador = num1.numerador * num2.denominador;
    int denominador = num1.denominador * num2.numerador;
  
    printf("Division: %d/%d \n ", numerador,denominador);
    simplificar(numerador,denominador);
}


void simplificar(int M, int N){
  int i,menor, multiplo, hay_multiplo;
  

  printf("%d/%d \n", M,N);
  do {
    if (M < N )
       menor = M;
    else
       menor = N;
    
    i=2;
    hay_multiplo=0;
    while (i <= menor && hay_multiplo == 0){
      if (M % i == 0 && N % i == 0){
         multiplo = i;
         hay_multiplo = 1;
      }
      i++;
    }
    if (hay_multiplo == 1){
       M=M/multiplo;
       N=N/multiplo;
       
       if (N==1){
       printf("Simplificando: %d \n", M);
       }else{
       printf("Simplificando:  %d/%d \n", M, N);
       }
    }
  }while (hay_multiplo==1);

}
 
int main() {
    operaciones();
    return 0;
}

  #include <stdio.h>
 



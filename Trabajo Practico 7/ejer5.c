
/*
 Puntos en un plano
Un punto en el plano se puede representar mediante una estructura con dos campos. Escribir
un programa que realice las siguientes operaciones con puntos en el plano:
● Dados dos puntos calcular la distancia entre ellos
● Dados dos puntos determinar la ecuación de la recta que pasa por ellos
● Dados tres puntos, que representan los vértices de un triángulo, calcular su área.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

struct cordenada {
    int x;
    int y;
};
void distancia(struct cordenada p1, struct cordenada p2);
void recta(struct cordenada p1, struct cordenada p2);
void area(struct cordenada p1, struct cordenada p2, struct cordenada p3);
double determinante(struct cordenada p1, struct cordenada p2, struct cordenada p3);

void operaciones() {
    struct cordenada p1;
    struct cordenada p2;
    struct cordenada p3;

    puts("Ingrese una cordenada X para el punto 1 \n");
    scanf("%d", &p1.x);
    puts("Ingrese una cordenada Y para el punto 1\n");
    scanf("%d", &p1.y);

    puts("Ingrese una cordenada X para el punto 2 \n");
    scanf("%d", &p2.x);
    puts("Ingrese una cordenada Y para el punto 2\n");
    scanf("%d", &p2.y);
    puts("A continuacion se pedida una cordenad para calcular el area de un triangulo.\n");
    puts("Ingrese una cordenada X para el punto 3 \n");
    scanf("%d", &p3.x);
    puts("Ingrese una cordenada Y para el punto 3\n");
    scanf("%d", &p3.y);


    distancia(p1, p2);
    recta(p1, p2);
    area(p1, p2, p3);



}

void distancia(struct cordenada p1, struct cordenada p2) {
    float distancia = sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
    printf("La distacia entre le punto 1 y 2 es: %.1f [L]:\n", distancia);
}

void recta(struct cordenada p1, struct cordenada p2) {
    float pendiente = (p1.y - p2.y) / (p1.x - p2.x);
    float b = p2.y - p1.x*pendiente;
    printf("la recta que pasa por el punto 1 y punto 2, es: y=%.1f x+%.1f\n", pendiente, b);

}

void area(struct cordenada p1, struct cordenada p2, struct cordenada p3) {

    double area =(int) determinante(p1, p2, p3) / 2;
    
    printf("Area de triangulo de los tres puntos: %.2f[L^2]\n", area);

}

double determinante(struct cordenada p1, struct cordenada p2, struct cordenada p3) {
    double deter;

    float a1 = p1.x * p2.y;
    float a2 = p2.x * p3.y;
    float a3 = p1.y * p3.x;
    float a4 = -p2.y * p3.x;
    float a5 = -p1.x * p3.y;
    float a6 = -p1.y * p2.x;

    deter = a1 + a2 + a3 + a4 + a5 + a6;

    return deter;
}

int main() {
    operaciones();
    return 0;
}



